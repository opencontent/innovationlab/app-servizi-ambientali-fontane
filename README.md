# Titolo
App che mostra le informazioni sulle fontane presenti nel bellunese.

## Descrizione 
Maggiori informazioni sono disponibili sul [sito del progetto](https://innovationlabdolomiti.openpa.opencontent.io/App).

## Altri riferimenti

[Dataset](https://api.opencontent.it/belluno/)


## Struttura del/dei repository
Il software è realizzato con Ionic 5 con base Angular 13

## Requisiti
Lo stack applicativo è composto da:
- NodeJs 14
- Ionic 5
- Angular 13


## Installazione
     npm install -g ionic
     npm install

## Project status 
Il progetto è stabile e usato in [produzione](https://opencontent.gitlab.io/innovationlab/app-servizi-ambientali-fontane/)

## CI
Mediante il servizio di CI di GitLab vengono preparate le immagini docker di ogni componente.

## Copyright
Copyright (C) 2016-2022  Opencontent SCARL
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Sicurezza
Inviare segnalazioni relative alla sicurezza di questo applicativo all'indirizzo e-mail security@opencontent.it
